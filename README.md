## ASSUMPTIONS

1. I used an interface to place on it the primary requirements of the library such us StartMatch, FinishMatch, UpdateMatch and SortGamesByTotalScore.  
I made this decision thinking in a future expansion of the code and differents implementations.  
For example, if you want to store the data in a data base instead of in a dictionary, you only have to create another class that implements this interface without modify the existing class ServiceGame.
	
2. I used a factory with a singleton to be the only one in the assembly with the possibilty to create a match.  
Furthermore, it is a way to test the parameters in the constructor only once. I mean, each time I create a match I do not have to check the Id, the Scores and the rest  
of the parameters again and again. The Factory is responsible of doing these checks.	
	
3. I assumed that all matches have to have an Id as unique key to ease the search of a match inside the collection.

4. I used a Dictionary <int,Match> to store the data because is the easiest way to do it with a primary key.

5. When I finished the unit tests, I noticed that I needed a method called GetMatch to test in a best way the correct creation and finalization of matches.  
I did not put in the interface because for me is not a primary requirement and may not be useful for others implementations.   
   
6. I used .NET 5.0 for compatibility and because it is multiplatform and xunit for testing because is a way to test in .NET.

7. When I finished everything, I noticed that to service was not respecting the SOLID principles because it had two different responsabilities: the business logic and the storage responsabolity.  
So, I decided to refactor the service to extract the storage responsability into the repository class.
