﻿using ScoreBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Repository
{
    public class MatchRepository : IMatchRepository
    {
        private Dictionary<int, Match> matches = new Dictionary<int, Match>();

        public void Add(int id, Match match)
        {
            matches.Add(id, match);
        }

        public bool GameExists(int id)
        {
            return matches.ContainsKey(id);
        }

        public List<Match> GetMatches()
        {
            return matches.Values.ToList();
        }

        public bool Remove(int id)
        {
            return matches.Remove(id);
        }

        public void Update(int id, int homeScore, int awayScore)
        {
            Match match = matches[id];
            match.ScoreHomeTeam = homeScore;
            match.ScoreAwayTeam = awayScore;
        }
    }
}
