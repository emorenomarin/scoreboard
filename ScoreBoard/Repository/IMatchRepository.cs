﻿using ScoreBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Repository
{
    public interface IMatchRepository
    {
        /// <summary>
        /// Determines if a game exists in the repository.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <returns>True if the game exists. False, otherwise.</returns>
        bool GameExists(int id);

        /// <summary>
        /// Rmoves a game from the repository.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <returns>True if the game is successfully removes. False, otherwise.</returns>
        bool Remove(int id);

        /// <summary>
        /// Adds a game into the repository.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <param name="match">The match to add.</param>
        void Add(int id, Match match);

        /// <summary>
        /// Update a game in the respository.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <param name="homeScore">Score of the home team.</param>
        /// <param name="awayScore">Score of the away team.</param>
        void Update(int id, int homeScore, int awayScore);

        /// <summary>
        /// List of matches in the repository.
        /// </summary>
        /// <returns>The list of matches.</returns>
        List<Match> GetMatches();
    }
}
