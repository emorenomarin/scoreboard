﻿using ScoreBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Service
{
    public class MatchFactory
    {
        private static MatchFactory instance;

        private MatchFactory()
        {
        }

        public static MatchFactory GetInstance()
        {
            if (instance == null)
            {
                instance = new MatchFactory();
            }
            return instance;
        }

        public Match CreateGame(int id, string homeTeam, string awayTeam)
        {
            if (id > 0 && homeTeam != null && awayTeam != null
                && !homeTeam.Equals(awayTeam))
            {
                return new Match(id, homeTeam, awayTeam);
            }
            else
            {
                return null;
            }
        }
    }
}
