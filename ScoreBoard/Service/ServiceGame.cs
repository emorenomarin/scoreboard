﻿using ScoreBoard.Model;
using ScoreBoard.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Service
{
    public class ServiceGame : IServiceGame
    {
        private IMatchRepository matchRepository;

        public ServiceGame(IMatchRepository matchRepository)
        {
            this.matchRepository = matchRepository;
        }

        public bool GameExists(int id)
        {
            return matchRepository.GameExists(id);
        }

        public bool FinishGame(int id)
        {
            if (matchRepository.GameExists(id))
            {
                return matchRepository.Remove(id);
            }

            return false;
        }

        public List<Match> SortGamesByTotalScore()
        {
            return (matchRepository.GetMatches().OrderByDescending(x => x.ScoreHomeTeam + x.ScoreAwayTeam).ThenByDescending(x => x.StartingTime)).ToList();
        }

        public bool StartGame(int id, string homeTeam, string awayTeam)
        {
            if (!matchRepository.GameExists(id))
            {
                MatchFactory matchFactory = MatchFactory.GetInstance();
                Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

                if (match != null)
                {
                    matchRepository.Add(id, match);
                    return true;
                }
            }

            return false;
        }

        public bool UpdateScore(int id, int scoreHomeTeam, int scoreAwayTeam)
        {
            if (matchRepository.GameExists(id))
            {
                matchRepository.Update(id, scoreHomeTeam, scoreAwayTeam);
                return true;
            }

            return false;
        }
    }
}
