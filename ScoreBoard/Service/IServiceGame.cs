﻿using ScoreBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Service
{
    public interface IServiceGame
    {
        /// <summary>
        /// Starts a game with the initial score 0-0.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <param name="homeTeam">Name of the home team.</param>
        /// <param name="awayTeam">Name of the away team.</param>
        /// <returns>True if the game is successfully started. False otherwise.</returns>
        bool StartGame(int id, string homeTeam, string awayTeam);

        /// <summary>
        /// Finishes a game that already started.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <returns>True if the game is successfully finished. False otherwise.</returns>
        bool FinishGame(int id);

        /// <summary>
        /// Updates a game with the provided score.
        /// </summary>
        /// <param name="id">Unique identifier of the game.</param>
        /// <param name="scoreHomeTeam">Score of the home team.</param>
        /// <param name="scoreAwayTeam">Score of the away team.</param>
        /// <returns>True if the game is successfully updated. False otherwise.</returns>
        bool UpdateScore(int id, int scoreHomeTeam, int scoreAwayTeam);

        /// <summary>
        /// Sorts games by total score and, in case of conflict, by the most recently added to our system.
        /// </summary>
        /// <returns>The list of games sorted.</returns>
        List<Match> SortGamesByTotalScore();
    }
}
