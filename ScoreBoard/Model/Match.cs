﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreBoard.Model
{
    public class Match
    {
        private int id;
        private string homeTeam;
        private string awayTeam;
        private int scoreHomeTeam;
        private int scoreAwayTeam;
        private DateTime startingTime;

        internal Match(int id, string homeTeam, string awayTeam)
        {
            this.id = id;
            this.homeTeam = homeTeam;
            this.awayTeam = awayTeam;
            this.scoreHomeTeam = 0;
            this.scoreAwayTeam = 0;
            this.startingTime = DateTime.Now;
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public string HomeTeam
        {
            get
            {
                return this.homeTeam;
            }
        }

        public string AwayTeam
        {
            get
            {
                return this.awayTeam;
            }
        }

        public int ScoreHomeTeam
        {
            get
            {
                return this.scoreHomeTeam;
            }
            set
            {
                this.scoreHomeTeam = value;
            }
        }

        public int ScoreAwayTeam
        {
            get
            {
                return this.scoreAwayTeam;
            }
            set
            {
                this.scoreAwayTeam = value;
            }
        }

        public DateTime StartingTime
        {
            get
            {
                return this.startingTime;
            }
        }
    }
}
