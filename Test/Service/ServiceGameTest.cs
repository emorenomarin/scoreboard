using ScoreBoard.Model;
using ScoreBoard.Repository;
using ScoreBoard.Service;
using System;
using System.Collections.Generic;
using Xunit;

namespace ScoreBoard.Test.Service
{
    public class ServiceGameTest
    {
        [Fact]
        public void StartingAGameCorrectly()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";

            bool matchStarted = serviceGame.StartGame(id, homeTeam, awayTeam);

            Assert.True(serviceGame.GameExists(id));
            Assert.True(matchStarted);
        }

        [Fact]
        public void NotStartingAGameWithTheSameID()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int idFirstGame = 1;
            string homeTeamFirstGame = "Mexico";
            string awayTeamFirstGame = "Canada";

            bool matchStarted = serviceGame.StartGame(idFirstGame, homeTeamFirstGame, awayTeamFirstGame);

            int idSecondGame = 1;
            string homeTeamSecondGame = "Spain";
            string awayTeamSecondGame = "Brazil";
            bool matchAlreadyStarted = serviceGame.StartGame(idSecondGame, homeTeamSecondGame, awayTeamSecondGame);

            Assert.False(matchAlreadyStarted);
        }

        [Fact]
        public void StartingAGameWithDifferentsID()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int idFirstGame = 1;
            string homeTeamFirstGame = "Mexico";
            string awayTeamFirstGame = "Canada";

            bool matchStarted = serviceGame.StartGame(idFirstGame, homeTeamFirstGame, awayTeamFirstGame);

            int idSecondGame = 3;
            string homeTeamSecondGame = "Spain";
            string awayTeamSecondGame = "Brazil";
            bool matchAlreadyStarted = serviceGame.StartGame(idSecondGame, homeTeamSecondGame, awayTeamSecondGame);

            Assert.True(matchAlreadyStarted);
        }

        [Fact]
        public void MatchFinishedCorrectly()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";

            bool matchStarted = serviceGame.StartGame(id, homeTeam, awayTeam);
            bool matchFinished = serviceGame.FinishGame(id);

            Assert.False(serviceGame.GameExists(id));
            Assert.True(matchFinished);
        }

        [Fact]
        public void FinishingAMatchThatDoesNotExist()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;

            bool matchFinished = serviceGame.FinishGame(id);

            Assert.False(matchFinished);
        }

        [Fact]
        public void UpdatingAGameWithAHomeScoreLowerThanZero()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            int homeScore = -1;
            int awayScore = 5;

            bool matchUpdated = serviceGame.UpdateScore(id, homeScore, awayScore);

            Assert.False(matchUpdated);
        }

        [Fact]
        public void UpdatingAGameWithAAwayScoreLowerThanZero()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            int homeScore = 1;
            int awayScore = -2;

            bool matchUpdated = serviceGame.UpdateScore(id, homeScore, awayScore);

            Assert.False(matchUpdated);
        }


        [Fact]
        public void UpdatingAMatchThatDoesNotExist()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            int homeScore = 0;
            int awayScore = 5;

            bool matchUpdated = serviceGame.UpdateScore(id, homeScore, awayScore);

            Assert.False(matchUpdated);
        }

        [Fact]
        public void UpdatingAMatchCorrectly()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";
            int homeScore = 0;
            int awayScore = 5;

            bool matchStarted = serviceGame.StartGame(id, homeTeam, awayTeam);
            bool matchUpdated = serviceGame.UpdateScore(id, homeScore, awayScore);

            Assert.True(matchUpdated);
        }

        [Fact]
        public void ReturningAnEmptyCollectionGames()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());

            List<Match> matchesSorted = serviceGame.SortGamesByTotalScore();

            Assert.Empty(matchesSorted);
        }

        [Fact]
        public void ReturningACollectionWithGames()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());

            bool matchStarted1 = serviceGame.StartGame(1, "Mexico", "Canada");
            bool matchStarted2 = serviceGame.StartGame(2, "Spain", "Brazil");

            List<Match> matchesSorted = serviceGame.SortGamesByTotalScore();

            Assert.NotEmpty(matchesSorted);
        }

        [Fact]
        public void SortingGamesWithDifferentScores()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());

            bool matchStarted1 = serviceGame.StartGame(1, "Mexico", "Canada");
            bool matchStarted2 = serviceGame.StartGame(2, "Spain", "Brazil");
            bool updatedMatch1 = serviceGame.UpdateScore(1, 0, 5);
            bool updatedMatch2 = serviceGame.UpdateScore(2, 10, 2);

            List<Match> matchesSorted = serviceGame.SortGamesByTotalScore();

            Assert.Equal(2, matchesSorted[0].Id);
            Assert.Equal(1, matchesSorted[1].Id);
            Assert.Equal(2, matchesSorted.Count);
        }

        [Fact]
        public void SortingGamesWithTheSameScore()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());

            bool matchStarted1 = serviceGame.StartGame(1, "Mexico", "Canada");
            System.Threading.Thread.Sleep(50);
            bool matchStarted2 = serviceGame.StartGame(2, "Spain", "Brazil");
            bool updatedMatch1 = serviceGame.UpdateScore(1, 0, 5);
            bool updatedMatch2 = serviceGame.UpdateScore(2, 0, 5);

            List<Match> matchesSorted = serviceGame.SortGamesByTotalScore();

            Assert.Equal(2, matchesSorted[0].Id);
            Assert.Equal(1, matchesSorted[1].Id);
            Assert.Equal(2, matchesSorted.Count);
        }

        [Fact]
        public void SortingGamesWithSimilarScoreAndAnotherOneWithDifferent()
        {
            ServiceGame serviceGame = new ServiceGame(new MatchRepository());

            bool matchStarted1 = serviceGame.StartGame(1, "Mexico", "Canada");
            System.Threading.Thread.Sleep(50);
            bool matchStarted2 = serviceGame.StartGame(2, "Spain", "Brazil");
            bool matchStarted3 = serviceGame.StartGame(3, "Uruguay", "Italy");
            bool updatedMatch1 = serviceGame.UpdateScore(1, 0, 5);
            bool updatedMatch2 = serviceGame.UpdateScore(2, 0, 5);
            bool updatedMatch3 = serviceGame.UpdateScore(3, 6, 6);

            List<Match> matchesSorted = serviceGame.SortGamesByTotalScore();

            Assert.Equal(3, matchesSorted[0].Id);
            Assert.Equal(2, matchesSorted[1].Id);
            Assert.Equal(1, matchesSorted[2].Id);
            Assert.Equal(3, matchesSorted.Count);
        }
    }
}
