﻿using ScoreBoard.Model;
using ScoreBoard.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ScoreBoard.Test.Service
{
    public class MatchFactoryTest
    {
        [Fact]
        public void IdGreaterThanZero()
        {
            MatchFactory matchFactory = MatchFactory.GetInstance();
            int id = 0;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";

            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            Assert.Null(match);
        }

        [Fact]
        public void HomeTeamNotNull()
        {
            MatchFactory matchFactory = MatchFactory.GetInstance();
            int id = 1;
            string homeTeam = null;
            string awayTeam = "Canada";

            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            Assert.Null(match);
        }

        [Fact]
        public void AwayTeamNotNull()
        {
            MatchFactory matchFactory = MatchFactory.GetInstance();
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = null;

            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            Assert.Null(match);
        }

        [Fact]
        public void NotEqualsTeam()
        {
            MatchFactory matchFactory = MatchFactory.GetInstance();
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Mexico";

            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            Assert.Null(match);
        }
    }
}
