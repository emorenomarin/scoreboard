﻿using ScoreBoard.Model;
using ScoreBoard.Repository;
using ScoreBoard.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ScoreBoard.Test.Service
{
    public class MatchRepositoryTest
    {
        [Fact]
        public void AddingAGameCorrectly()
        {
            MatchRepository matchRepository = new MatchRepository();
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";
            MatchFactory matchFactory = MatchFactory.GetInstance();
            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            matchRepository.Add(id, match);

            Assert.True(matchRepository.GameExists(id));
        }

        [Fact]
        public void RemovingAGameCorrectly()
        {
            MatchRepository matchRepository = new MatchRepository();
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";
            MatchFactory matchFactory = MatchFactory.GetInstance();
            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            matchRepository.Add(id, match);

            Assert.True(matchRepository.Remove(id));
        }

        [Fact]
        public void UpdatingAMatchCorrectly()
        {
            MatchRepository matchRepository = new MatchRepository();
            int id = 1;
            string homeTeam = "Mexico";
            string awayTeam = "Canada";
            int homeScore = 0;
            int awayScore = 5;

            MatchFactory matchFactory = MatchFactory.GetInstance();
            Match match = matchFactory.CreateGame(id, homeTeam, awayTeam);

            matchRepository.Add(id, match);
            matchRepository.Update(id, homeScore, awayScore);

            Assert.Equal(0, matchRepository.GetMatches()[0].ScoreHomeTeam);
            Assert.Equal(5, matchRepository.GetMatches()[0].ScoreAwayTeam);
        }
    }
}
